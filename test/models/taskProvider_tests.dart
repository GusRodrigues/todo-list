import 'package:flutter_test/flutter_test.dart';
import 'package:todoApp/models/task.dart';
import 'package:todoApp/models/task_provider.dart';

void main() {
  TaskProvider testClass;
  Task mocked;

  setUp(() {
    testClass = TaskProvider();
    mocked = Task(name: 'test', description: 'case');
  });

  tearDown(() {
    testClass = null;
    mocked = null;
  });

  group('TaskProvider test cases', () {
    test('initial state', () {
      List<Task> expected = [];
      var actual = testClass.tasks;
      expect(actual, equals(expected));
      expect(actual.length, isZero);
    });

    test('Can add Task', () {
      List<Task> expected = [mocked];
      testClass.addTask(mocked);
      var actual = testClass.tasks;
      expect(actual, equals(expected));
      expect(actual.length, equals(expected.length));
    });

    test('Can get an object from the tasks', () {
      testClass.addTask(mocked);
      Task item = testClass.tasks[0];
      expect(item, isNotNull);
      expect(item, equals(mocked));
    });

    test('Can remove an object from the tasks', () {
      testClass.addTask(mocked);
      expect(testClass.removeTask(mocked), isTrue);
    });

    test('Cannot remove an object from an empty task list', () {
      expect(testClass.removeTask(mocked), isFalse);
    });

    test('Can change the state of a Task ', () {
      testClass.addTask(mocked);
      testClass.tasks[0].toggleDone();

      Task testCase = testClass.tasks[0];

      expect(testCase.isDone, isTrue);
    });
  });
}
