import 'package:flutter_test/flutter_test.dart';
import 'package:todoApp/models/task.dart';

void main() {
  group('Task test cases', () {
    Task task;
    setUp(() => task = Task(name: 'Test', description: 'case'));
    tearDown(() => task = null);

    test('Malformation causes exception', () {
      expect(() => Task(name: null), throwsA(isA<AssertionError>()));
      expect(() => Task(name: ''), throwsA(isA<AssertionError>()));
      expect(() => Task(name: '  '), throwsA(isA<AssertionError>()));
    });

    test('No description task', () {
      String expected = '[Task: Test]';
      task = Task(name: 'Test');
      expect(task.toString(), equals(expected));
    });

    test('Task with description', () {
      String expected = '[Task: Test desc: case ]';
      task = Task(name: 'Test', description: 'case');
      expect(task.toString(), equals(expected));
    });

    test('Can change state of isDone', () {
      final bool done = task.isDone;
      expect(done, isFalse);
      task.toggleDone();
      expect(task.isDone, isTrue);
      expect(task.isDone, isNot(equals(done)));
    });
  });
}
