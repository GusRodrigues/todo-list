import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoApp/models/task_provider.dart';
import 'package:todoApp/views/task_screen.dart';

void main() => runApp(ChangeNotifierProvider(
    create: (context) => TaskProvider(), child: MyApp()));

class MyApp extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return MaterialApp(
      theme: ThemeData.light().copyWith(
          primaryColor: Colors.lightGreen,
          accentColor: Colors.red.shade700,
          hintColor: Colors.indigo,
          scaffoldBackgroundColor: Colors.white,
          appBarTheme:
              AppBarTheme().copyWith(color: Colors.lightGreen, elevation: 4)),
      home: TaskView(),
    );
  }
}
