import 'package:flutter/material.dart';
import 'package:todoApp/views/add_task_screen.dart';
import 'package:todoApp/widgets/task_element.dart';
import 'package:todoApp/widgets/todo_AppBar_element.dart';

class TaskView extends StatelessWidget {
  final _scaffoldKey = GlobalKey<ScaffoldState>();
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      key: _scaffoldKey,
      appBar: todoAppBar(context),
      // The Task List
      body: SafeArea(
        top: true,
        minimum: const EdgeInsets.all(8),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          crossAxisAlignment: CrossAxisAlignment.stretch,
          mainAxisAlignment: MainAxisAlignment.start,
          children: <Widget>[
            Expanded(
              child: TaskList(),
            ),
          ],
        ),
      ),
      // The + FAB
      floatingActionButton: FloatingActionButton(
        onPressed: () => addTaskPresenter(context),
        elevation: 2,
        backgroundColor: Theme.of(context).hintColor,
        child: Icon(Icons.add),
        tooltip: 'Add item',
      ),
    );
  }

  /// Calls the AddTask form into view using a Modal [BottomSheet] that is the FAB will invoke
  Future addTaskPresenter(BuildContext context) {
    return showModalBottomSheet(
      isScrollControlled: true,
      backgroundColor: Colors.white,
      context: context,
      builder: (context) => SingleChildScrollView(child: AddTask()),
    );
  }
}
