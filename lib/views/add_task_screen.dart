import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoApp/models/task.dart';
import 'package:todoApp/models/task_provider.dart';

/// The half screen page that presents the form
class AddTask extends StatefulWidget {
  @override
  _AddTaskState createState() => _AddTaskState();
}

class _AddTaskState extends State<AddTask> {
  final _textNameController = TextEditingController();
  final _textDescriptionController = TextEditingController();

  final _formKey = GlobalKey<FormState>();

  @override
  void dispose() {
    _textNameController.dispose();
    _textDescriptionController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Form(
      key: _formKey,
      child: Padding(
        padding: EdgeInsets.fromLTRB(
            16, 16, 16, MediaQuery.of(context).viewInsets.bottom),
        child: Column(
          mainAxisSize: MainAxisSize.min,
          mainAxisAlignment: MainAxisAlignment.start,
          crossAxisAlignment: CrossAxisAlignment.center,
          children: <Widget>[
            Text(
              'Add Task',
              style: TextStyle(
                fontSize: 24,
                color: Theme.of(context).primaryColor,
                fontWeight: FontWeight.w300,
              ),
            ),
            TextFormField(
              controller: _textNameController,
              decoration: InputDecoration(labelText: 'Task*'),
              textAlign: TextAlign.center,
              maxLength: 32,
              validator: (v) => _inputValidation(
                v,
                error: 'The name of the task is mandatory.',
              ),
            ),
            TextFormField(
              controller: _textDescriptionController,
              textAlign: TextAlign.center,
              maxLength: 64,
              decoration: InputDecoration(labelText: 'Task description'),
            ),
            SizedBox(
              height: 16,
            ),
            RaisedButton(
              onPressed: () {
                if (_formKey.currentState.validate()) {
                  // push the task to the task list.
                  Provider.of<TaskProvider>(context, listen: false).addTask(
                      Task(
                          name: _textNameController.text,
                          description: _textDescriptionController.text));
                  Navigator.pop(context);
                }
              },
              color: Theme.of(context).primaryColor,
              elevation: 4,
              child: Text(
                'Submit',
                style: TextStyle(color: Colors.white),
              ),
            ),
          ],
        ),
      ),
    );
  }

  /// An input is valid iff it is not empty, resulting in a null
  /// An invalid input will cause the return of the desired error message.
  String _inputValidation(String input, {String error}) {
    if (input.isEmpty) return error;
    return null;
  }
}
