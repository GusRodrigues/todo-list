/// Task is a mutable instance representing a task.
/// Task requires a Non-null or white spaced name and description.
/// The name of the task also requires no more than 32 characters
/// If no description is given, the default is set (an empty string).
class Task {
  final String name;
  final String description;
  bool _isDone = false;

  Task({this.name, this.description = ''})
      : assert(name != null && name.trim().isNotEmpty,
            'Task cannot be null or empty'),
        assert(name.length < 32, 'Task name is too long');

  bool get isDone => _isDone; // encapsulate property

  /// change the value if the Task is done or not
  void toggleDone() => _isDone = !_isDone; // mutator

  @override
  int get hashCode {
    int prime = 79;
    int hash = 19 * prime + name.hashCode;
    hash += 19 + description.hashCode * prime;
    return hash;
  }

  @override
  bool operator ==(other) {
    if (other is! Task) return false;
    Task that = other;

    return (that.name == name && that.description == description);
  }

  @override
  String toString() {
    if (description == null || description.isEmpty)
      return '[Task: ' + name + ']';
    return '[Task: ' + name + ' desc: ' + description + ' ]';
  }
}
