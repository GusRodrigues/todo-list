import 'package:flutter/foundation.dart';
import 'package:todoApp/models/task.dart';

class TaskProvider extends ChangeNotifier {
  List<Task> _tasks = [];
  List<Task> get tasks => List.of(_tasks);
  int get size => _tasks.length;

  /// Expose add element
  void addTask(Task toAdd) {
    _tasks.add(toAdd);
    notifyListeners();
  }

  /// Expose remove element
  bool removeTask(Task toRemove) {
    var attempt = _tasks.remove(toRemove);
    if (attempt) {
      notifyListeners();
      return true;
    } else {
      return false;
    }
  }
}
