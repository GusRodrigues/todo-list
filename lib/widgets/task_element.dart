import 'package:flutter/material.dart';
import 'package:provider/provider.dart';
import 'package:todoApp/models/task.dart';
import 'package:todoApp/models/task_provider.dart';

/// Task Elements holds the Generator of the List and How they are displayed
class TaskList extends StatefulWidget {
  @override
  _TaskListState createState() => _TaskListState();
}

class _TaskListState extends State<TaskList> {
  List<Task> _tasks;

  @override
  void didChangeDependencies() {
    super.didChangeDependencies();
    _tasks = Provider.of<TaskProvider>(context).tasks;
  }

  @override
  Widget build(BuildContext context) => ListView.separated(
        shrinkWrap: true,
        physics: BouncingScrollPhysics(),
        itemCount:
            _tasks.length, // TODO implement call to build based on size on file
        itemBuilder: (context, i) {
          final task = _tasks[i];
          return Dismissible(
            key: Key(task.toString()),
            onDismissed: (direction) {
              setState(() {
                Provider.of<TaskProvider>(context, listen: false)
                    .removeTask(task);
                Scaffold.of(context).showSnackBar(SnackBar(
                  content: Row(
                    children: <Widget>[
                      Text(
                        'Task: ${task.name}',
                        style: TextStyle(color: Theme.of(context).primaryColor),
                      ),
                      Text(' removed'),
                    ],
                  ),
                ));
              });
            },
            child: TaskItem(
              task: task, // TODO Read the Task
            ),
          );
        },
        separatorBuilder: (_, index) => Divider(),
      );
}

/// Task Item manages it own state
class TaskItem extends StatefulWidget {
  const TaskItem({
    Key key,
    @required this.task,
  }) : super(key: key);

  final Task task;

  @override
  _TaskItemState createState() => _TaskItemState();
}

class _TaskItemState extends State<TaskItem> {
  bool _checked;
  String _name;
  String _desc;

  @override
  void initState() {
    super.initState();
    _checked = widget.task.isDone;
    _name = widget.task.name;
    _desc = widget.task.description;
    (_desc == null || _desc == '') ? _desc = '' : _desc = _desc;
  }

  /// Flips _checked and the toggle task as done
  void _checkboxCallback() => setState(() {
        _checked = !_checked;
        widget.task.toggleDone();
      });

  /// Defines if the text in the Title and Description will be strike through
  TextStyle _crossText() => (_checked)
      ? TextStyle(
          decoration: TextDecoration.lineThrough,
          color: Theme.of(context).accentColor,
          fontWeight: FontWeight.bold,
        )
      : TextStyle(
          decoration: TextDecoration.none, fontWeight: FontWeight.normal);

  @override
  Widget build(BuildContext context) => ListTile(
      leading: Icon(Icons.more_vert),
      title: Text(_name, style: _crossText()),
      subtitle: Text(_desc, style: _crossText()),
      trailing: TaskCheckbox(
        callback: _checkboxCallback,
        checked: _checked,
      ));
}

/// TaskCheckBox will callback if is pressed
class TaskCheckbox extends StatelessWidget {
  /// Callback the function handling the [TaskItem] State
  final Function callback;
  final bool checked;

  const TaskCheckbox({this.callback, this.checked});

  @override
  Widget build(BuildContext context) => Checkbox(
        value: checked,
        activeColor: Theme.of(context).accentColor,
        onChanged: (_) => callback(),
      );
}
