import 'package:flutter/material.dart';
import 'package:todoApp/models/task_provider.dart';
import 'package:provider/provider.dart';

PreferredSizeWidget todoAppBar(BuildContext context) {
  String _titleText = 'ToDo List';
  int count = Provider.of<TaskProvider>(context).tasks.length;

  Widget _subtitle() {
    var output = '';
    if (count == 0) {
      output = 'no tasks ToDo';
    } else {
      output = '$count tasks ToDo';
    }

    return Text(
      output,
      style: TextStyle(fontWeight: FontWeight.w300, fontSize: 16),
    );
  }

  Widget _title() {
    return Text(
      _titleText,
      style: TextStyle(fontWeight: FontWeight.w300, fontSize: 32),
    );
  }

  return PreferredSize(
    preferredSize: Size.fromHeight(72),
    child: AppBar(
      centerTitle: true,
      title: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _title(),
          _subtitle(),
        ],
      ),
      elevation: 8,
      shape: RoundedRectangleBorder(
        borderRadius: BorderRadius.vertical(
          bottom: Radius.circular(16),
        ),
      ),
    ),
  );
}
